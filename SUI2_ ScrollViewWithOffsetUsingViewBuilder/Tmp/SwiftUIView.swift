////  SwiftUIView.swift
//  SUI2_ ScrollViewWithOffsetUsingViewBuilder
//
//  Created on 02/04/2021.
//  
//

import SwiftUI

struct SwiftUIView: View {
    var body: some View {
        Image(systemName: "bell")
            .resizable()
            .scaledToFit()
                .overlay(
                    Text("Mac OS Catalina")
                    .foregroundColor(.gray)
                    .font(.largeTitle)
                    ,alignment: .center)
    }
}

struct SwiftUIView_Previews: PreviewProvider {
    static var previews: some View {
        SwiftUIView()
    }
}
