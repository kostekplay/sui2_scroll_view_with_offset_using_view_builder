////  ContentView.swift
//  SUI2_ ScrollViewWithOffsetUsingViewBuilder
//
//  Created on 02/04/2021.
//  
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        HomeView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct HomeView: View {
    
    @State var offset: CGPoint = .zero
    
    var body: some View {
        NavigationView {
            CustomScrollView(offset: $offset, showIndictors: true, axis: .vertical, content: {
                VStack(spacing: 15, content: {
                    ForEach(1...50, id: \.self) { _ in
                        HStack(spacing: 15, content: {
                            Circle()
                                .fill(Color.gray.opacity(0.6))
                                .frame(width: 30, height: 30)
                            
                            VStack(alignment: .leading, spacing: 15, content: {
                                
                                Rectangle()
                                    .fill(Color.gray.opacity(0.6))
                                    .frame(height: 15)
                                
                                Rectangle()
                                    .fill(Color.gray.opacity(0.6))
                                    .frame(height: 15)
                                    .padding(.trailing,90)
                            })
                        })
                        .padding(.horizontal)
                    }
                })
                .padding(.top)
            })
            .navigationTitle("Offset: \(String(format: "%.1f", offset.y))")
        }
    }
}

struct CustomScrollView<Content: View>: View {
    
    @Binding var offset: CGPoint
    
    var showIndictors: Bool
    var axis: Axis.Set
    
    var content: Content
    
    init(offset: Binding<CGPoint>, showIndictors: Bool, axis: Axis.Set, @ViewBuilder content: ()->Content) {
        
        self._offset = offset
        self.showIndictors = showIndictors
        self.axis = axis
        
        self.content = content()
        
    }
    
    @State var startOffset: CGPoint = .zero
    
    var body: some View {
        ScrollView(axis, showsIndicators: showIndictors, content: {
            content
            .overlay(
                GeometryReader{geometry -> Color in

                    let rect = geometry.frame(in: .global)
                    print("Rect -> \(rect)")

                    if startOffset == .zero{
                        DispatchQueue.main.async {
                            startOffset = CGPoint(x: rect.minX, y: rect.minY)
                        }
                    }

                    DispatchQueue.main.async {
                        self.offset = CGPoint(x: startOffset.x - rect.minX, y: startOffset.y - rect.minY)
                    }

                    return Color.clear
                }

                .frame(width: UIScreen.main.bounds.width, height: 0),alignment: .top
            )
        })
    }
}
