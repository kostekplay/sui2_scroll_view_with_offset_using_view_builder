////  SUI2__ScrollViewWithOffsetUsingViewBuilderApp.swift
//  SUI2_ ScrollViewWithOffsetUsingViewBuilder
//
//  Created on 02/04/2021.
//  
//

import SwiftUI

@main
struct SUI2__ScrollViewWithOffsetUsingViewBuilderApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
